import { Component, OnInit } from '@angular/core';
import { customerDetailsFormat } from './customerModel';

@Component({
  selector: 'app-directives',
  templateUrl: './directives.component.html',
  styleUrls: ['./directives.component.css']
})
export class DirectivesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  courseList = [1,2,3]
  hasStudents = true;
  color = "";
  changeColor(colorToChange:string){
    this.color = colorToChange;
  }
  studentList = ["Abhi","Alen","Minu"];
  customerList : customerDetailsFormat[] =[
    {name:"Abhi",age:30},
    {name:"Rabbi",age:32},
    {name:"ingu",age:36}
  ]
}
